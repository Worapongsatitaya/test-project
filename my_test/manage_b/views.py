from django.shortcuts import render
from manage_b.models import Category, Book

# Create your views here.
def index(request):
    context = {
        'cat_num': len(Category.objects.all()),
        'book_num' : len(Book.objects.all())
    }
    return render(request, 'index.html', context)
